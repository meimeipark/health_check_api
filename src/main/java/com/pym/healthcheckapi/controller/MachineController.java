package com.pym.healthcheckapi.controller;

import com.pym.healthcheckapi.model.MachineInfoChangeRequest;
import com.pym.healthcheckapi.model.MachineRequest;
import com.pym.healthcheckapi.model.MachineStaticsResponse;
import com.pym.healthcheckapi.service.MachineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/machine")
public class MachineController {
    private final MachineService machineService;

    @PostMapping("/new")
    public String setMachine (@RequestBody MachineRequest request){
        machineService.setMachine(request);

        return "등록 완료";
    }

    @GetMapping("/statics")
    public MachineStaticsResponse getStatics (){
        return machineService.getStatics();
    }

    @PutMapping("/info/{id}")
    public String putMachineInfo (@PathVariable long id, @RequestBody MachineInfoChangeRequest request){
        machineService.putMachineInfo(id, request);

        return "수정 완료";
    }
}
