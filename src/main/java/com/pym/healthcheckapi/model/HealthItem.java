package com.pym.healthcheckapi.model;

import com.pym.healthcheckapi.enums.HealthStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class HealthItem {
    private Long id;
    private LocalDate dateCreate;
    private String name;
    private String healthStatus;
    private Boolean isGoHome;
}
