package com.pym.healthcheckapi.model;

import com.pym.healthcheckapi.enums.HealthStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthRequest {
    private String name;
    private HealthStatus healthStatus;
    private String etcMemo;
    private Boolean isChronicDisease;
}
