package com.pym.healthcheckapi.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HealthResponse {
    private Long id;
    private LocalDate dateCreate;
    private String name;
    private String etcMemo;
    private String healthStatusName;
    private String isGoHomeName;
    private String isChronicDiseaseName;
}
