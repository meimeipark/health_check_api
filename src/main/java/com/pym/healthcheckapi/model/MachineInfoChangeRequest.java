package com.pym.healthcheckapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MachineInfoChangeRequest {
    private String machineName;
    private Double machinePrice;
}
