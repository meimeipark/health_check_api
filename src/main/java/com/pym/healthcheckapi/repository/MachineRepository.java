package com.pym.healthcheckapi.repository;

import com.pym.healthcheckapi.entity.Machine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MachineRepository extends JpaRepository <Machine, Long> {
}
