package com.pym.healthcheckapi.service;

import com.pym.healthcheckapi.entity.Machine;
import com.pym.healthcheckapi.model.MachineInfoChangeRequest;
import com.pym.healthcheckapi.model.MachineRequest;
import com.pym.healthcheckapi.model.MachineStaticsResponse;
import com.pym.healthcheckapi.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    public void setMachine (MachineRequest request){
        Machine addData = new Machine();
        addData.setMachineName(request.getMachineName());
        addData.setMachinePrice(request.getMachinePrice());
        addData.setDateBuy(request.getDateBuy());

        machineRepository.save(addData);
    }

    public MachineStaticsResponse getStatics(){
        MachineStaticsResponse response = new MachineStaticsResponse();

        List<Machine> originList = machineRepository.findAll();

        double totalPrice = 0D;
        double totalPressKg = 0D;

        for (Machine machine : originList) {
            totalPrice += machine.getMachinePrice();
            totalPressKg += machine.getMachineType().getPressKg();
        }

        double avgPrice = totalPrice / originList.size();

        response.setTotalPrice(totalPrice);
        response.setAvgPrice(avgPrice);

        return response;
    }

    public void putMachineInfo (long id, MachineInfoChangeRequest request){
        Machine originData = machineRepository.findById(id).orElseThrow();
        originData.setMachineName(request.getMachineName());
        originData.setMachinePrice(request.getMachinePrice());

        machineRepository.save(originData);
    }
}
